require 'pg'

conn = PG.connect( host: 'postgres', dbname: 'custom_db', user: 'custom_user', password: 'custom_pass' )
conn.exec( "SELECT 'OK' AS status;" ) { |result| p result }
